package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

/**
 * 拦截参数
 *
 * @author bwcx_jzy
 * @since 2021/11/8
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {

    /**
     * 允许访问的用户名 数组
     *
     * @return 可以访问的用户名数组
     */
    String[] value();

    /**
     * 验证前端插入的参数名
     *
     * @return 参数名
     */
    String paramName() default "username";
}
