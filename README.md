一、编程题

手写MVC框架基础上增加如下功能

1）定义注解@Security（有value属性，接收String数组），该注解用于添加在Controller类或者Handler方法上，表明哪些用户拥有访问该Handler方法的权限（注解配置用户名）

2）访问Handler时，用户名直接以参数名username紧跟在请求的url后面即可，比如http://localhost:8080/demo/handle01?username=zhangsan

3）程序要进行验证，有访问权限则放行，没有访问权限在页面上输出

注意：自己造几个用户以及url，上交作业时，文档提供哪个用户有哪个url的访问权限

二、作业资料说明：

1、提供资料：代码工程、验证及讲解视频。

2、讲解内容包含：题目分析、实现思路、代码讲解。

3、效果视频验证

1）展示关键实现代码

2）有访问权限则放行，没有访问权限在页面上输出


## 测试的 URL

## 可以访问

http://localhost:8080/demo/handle01?username=liyang

http://localhost:8080/demo/handle01?username=test

## 不可以访问

http://localhost:8080/demo/handle01?username=zhangsan