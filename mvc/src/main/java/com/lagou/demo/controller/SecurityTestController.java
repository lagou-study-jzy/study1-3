package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author bwcx_jzy
 * @since 2021/11/8
 */
@LagouController
@LagouRequestMapping("/demo")
public class SecurityTestController {

    @LagouAutowired
    private IDemoService demoService;


    /**
     * URL: /demo/handle01?username=lisi
     *
     * @param request  请求对象
     * @param response 响应
     * @param username 参数
     * @return 输出当前用户
     */
    @LagouRequestMapping("/handle01")
    @Security(value = {"liyang", "test"})
    public String handle01(HttpServletRequest request, HttpServletResponse response, String username) {
        return "当前用户" + demoService.get(username);
    }
}
