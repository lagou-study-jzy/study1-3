package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

/**
 * 依赖注入
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LagouAutowired {

    /**
     * bean name
     *
     * @return 依赖 bean 的名称
     */
    String value() default "";
}
