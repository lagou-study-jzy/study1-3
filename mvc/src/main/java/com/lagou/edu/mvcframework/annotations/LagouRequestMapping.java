package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

/**
 * mapping
 *
 * @author user
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LagouRequestMapping {

    /**
     * url
     *
     * @return url
     */
    String value() default "";
}
